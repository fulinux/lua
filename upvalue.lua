#!/usr/bin/lua

function fun()
    local iVal = 10 --upvalue

    function InnerFun1() --内嵌函数
        print(iVal)
    end

    function InnerFun2()
        iVal = iVal + 10
    end

    return InnerFun1, InnerFun2
end

local a, b = fun()

a()

b()

a()

