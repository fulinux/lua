/*********************************************************************************
 *      Copyright:  (C) 2015 NO
 *                  All rights reserved.
 *
 *       Filename:  average.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(2015年09月22日)
 *         Author:  fulinux <fulinux@sina.com>
 *      ChangeLog:  1, Release initial version on "2015年09月22日 23时41分56秒"
 *                 
 ********************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <lua5.1/lua.h>
#include <lua5.1/lauxlib.h>
#include <lua5.1/lualib.h>

lua_State *L;

static int average(lua_State *L)
{
    int i;
    double sum = 0;
    int n = lua_gettop(L);
    for(i = 1; i < n; i++){
        if(!lua_isnumber(L, i)){
            lua_pushstring(L, "incorret argument to 'average");
            lua_error(L);
        } else {
            sum += lua_tonumber(L, i);
        }
    }

    lua_pushnumber(L, sum/n);
    lua_pushnumber(L, sum);

    return 2;
}

int main (int argc, char **argv)
{
    L = lua_open();

    luaL_openlibs(L);

    lua_register(L, "average", average);

    luaL_dofile(L, "average.lua");

    lua_close(L);

    return 0;
} /* -----End of main()----- */

