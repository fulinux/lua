#!/usr/bin/lua

local function LanguageTest()
    -- table test
    local names = {"Peter", "Paul", "Mary"}
    local grades = {Mary = 10, Paul = 7, Peter = 8}
    table.sort(names, function(n1, n2)
        return grades[n1] > grades[n2]
    end)
    for i = 1, #names do
        print(names[i])
    end

    -- function test
    local function newCounter(name)
        local i = 0
        return function()
            i = i + 1
            return name ..":".. i
        end
    end

    local c1 = newCounter("c1")
    local c2 = newCounter("c2")

    print(c1())
    print(c1())
    print(c2())
    print(c1())
    print(c2())

    -- foe test
    local function values(t)
        local i = 0;
        return function() i = i + 1; return t[i] end
    end
    for elm in values(names) do
        print(elm)
    end
end

local function tableTest()
    local Set = {}
    local mt = {}

    -- create a new set with teh values of the given list
    Set.new = function(l)
        local set = {}
        setmetatable(set, mt)
        for _, v in ipairs(l) do set[v] = true end
        return set;
    end

    Set.union = function(a, b)
        if getmetatable(a) ~= mt or getmetatable(b) ~= mt then
            error("attempt to 'add' a set with a non-set value", 2);
        end
        local res = Set.new{}
        for k in pairs(a) do res[k] = true end
        for k in pairs(b) do res[k] = true end
        return res
    end

    Set.inter

