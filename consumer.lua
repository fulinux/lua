#!/usr/bin/lua

produceFunc = function()
    while true do
        local value = io.read()
        print("produce:", value)
        coroutine.yield(value)
    end
end

consumer = function(p)
    while true do
        local status, value = coroutine.resume(p);
        print("consume: ", value)
    end
end

producer = coroutine.create(produceFunc)

consumer(producer)
