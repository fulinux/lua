/*********************************************************************************
 *      Copyright:  (C) 2015 NO
 *                  All rights reserved.
 *
 *       Filename:  test.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(2015年09月22日)
 *         Author:  fulinux <fulinux@sina.com>
 *      ChangeLog:  1, Release initial version on "2015年09月22日 00时06分14秒"
 *                 
 ********************************************************************************/
#include <stdio.h>
#include <string.h>

#include <lua5.1/lua.h>
#include <lua5.1/lualib.h>
#include <lua5.1/lauxlib.h>

/* The lua interpreter */
lua_State *L;
int luaadd(int x, int y)
{
    int sum;

    /* the function name */
    lua_getglobal(L, "add");

    /* the first argument */
    lua_pushnumber(L, x);

    /* the second argument */
    lua_pushnumber(L, y);

    /* call the function with 2 arguments, return 1 result. */
    lua_call(L, 2, 1);

    /* get the result */
    sum = (int)lua_tonumber(L, -1);

    /* cleanup the return */
    lua_pop(L, 1);

    return sum;
}

int main (int argc, char **argv)
{
    int sum;
    /* initialize lua */
    L = lua_open();

    /* load lua base libraries */
    luaL_openlibs(L);

    /* load the script */
    luaL_dofile(L, "add.lua");

    /* call the add function */
    sum = luaadd(10, 15);

    /* print the result */
    printf("The sum is %d \n", sum);

    /* cleanup lua */
    lua_close(L);

    return 0;
} /* -----End of main()----- */
