/*********************************************************************************
 *      Copyright:  (C) 2015 NO
 *                  All rights reserved.
 *
 *       Filename:  table.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(2015年09月22日)
 *         Author:  fulinux <fulinux@sina.com>
 *      ChangeLog:  1, Release initial version on "2015年09月22日 18时44分32秒"
 *                 
 ********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <lua5.1/lua.h>
#include <lua5.1/lauxlib.h>
#include <lua5.1/lualib.h>

void load(lua_State *L)
{
    if (luaL_loadstring(L, "background = {r = 0.30, g = 0.10, b = 0}")
            || lua_pcall(L, 0, 0, 0)){
        printf("Error Msg is %s.\n", lua_tostring(L, -1));
        return;
    }
    lua_getglobal(L, "background");
    if(!lua_istable(L, -1)){
        printf("'background' is not a table.\n");
        return;
    }

    lua_getfield(L, -1, "r");
    if(!lua_isnumber(L, -1)){
        printf("Invalid component in background color");
    }

    int r = (int)(lua_tonumber(L, -1) * 255);
    lua_pop(L, 1);
    lua_getfield(L, -1, "g");
    if(!lua_isnumber(L, -1)){
        printf("Invaild component in background color.\n");
        return;
    }
    int g = (int)(lua_tonumber(L, -1) * 255);
    lua_pop(L, 1);

    lua_pushnumber(L, 0.4);
    lua_setfield(L, -2, "b");

    lua_getfield(L, -1, "b");
    if(!lua_isnumber(L, -1)){
        printf("Invalid component in background color.\n");
        return;
    }

    int b = (int)(lua_tonumber(L, -1) * 255);
    printf("r = %d, g = %d, b = %d\n", r, g, b);
    lua_pop(L, 1);
    lua_pop(L, 1);
    return;
}

int main (int argc, char **argv)
{
    lua_State *L = luaL_newstate();
    load(L);
    lua_close(L);

    return 0;
} /* -----End of main()----- */

