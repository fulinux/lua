#!/usr/bin/lua

function average(...)
    local sum = 0
    print("average...")
    for i, j in pairs(arg) do
        sum = sum + j
    end
    return sum/table.getn(arg);
end
