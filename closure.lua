#!/usr/bin/lua

function test(x)
    return function (value)
        return value * x
    end
end

func = test(10)

print(func(11))
