#!/usr/bin/lua

local mt = {}

mt.__add = function(s1, s2)
    local result = ""
    if s1.sex == "boy" and s2.sex == "girl" then
        result = "完美的家庭"
    elseif s1.sex == "girl" and s2.sex == "girl" then
        result = "哦呵呵"
    else
        result = "神经病"
    end

    return result;
end

local s1 = {
    name = "hello",
    sex = "boy",
}

local s2 = {
    name = "world",
    sex = "girl",
}

setmetatable(s1, mt)
setmetatable(s2, mt)

local result = s1 + s2

print(result)

local result = s2 + s1

print(result)
