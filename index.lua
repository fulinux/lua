#!/usr/bin/lua

windows = {}

windows.default = {x = 0, y = 0, width = 100, height = 100, color = {r = 255, g = 255, b = 255}}

windows.mt = {}

function windows.new(o)
    setmetatable(o, windows.mt)
    return o
end

windows.mt.__index = function(table, key)
    return windows.default[key]
end

local win = windows.new({x = 10, y = 10})

print(win.x)
print(win.width)
print(win.color.r)
