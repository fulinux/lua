#!/usr/bin/lua

function newCounter()
    local i = 0
    func = function()
        i = i + 1
        return i
    end
    return func
end

c = newCounter()
print(c())
print(c())

c1 = newCounter()
print(c1())
print(c1())

print(c())
print(c1())
