#!/usr/bin/lua

guild = {}

table.insert(guild, {
    name = "Cladhaire",
    class = "Rogue",
    level = 70,
})

table.insert(guild, {
    name = "sagart",
    class = "Priest",
    level = 70,
})

table.insert(guild, {
    name = "Mallaithe",
    class = "Warlock",
    level = 40,
})

function sortlevelNameAsc(a, b)
    if a.level == b.level then
        return a.name < b.name
    else
        return a.level < b.level
    end
end

table.sort(guild, sortlevelNameAsc)

for idx, value in ipairs(guild) do print(idx, value.name) end
