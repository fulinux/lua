#!/usr/bin/lua

local t = {}
print(getmetatable(t))

local t1 = {}

setmetatable(t, t1)
print(assert(getmetatable(t) == t1))

print(getmetatable("hello world"))
print(getmetatable(10))

